﻿using System.Globalization;
using Newtonsoft.Json;

namespace TimeTreess
{
    class Program
    {
        struct Person
        {
            public int Id;
            public string Name;
            public DateTime BirthDate;
            public DateTime DeathDate;

            public Person(int Id, string Name, DateTime BirthDate, DateTime DeathDate)
            {
                this.Id = Id;
                this.Name = Name;
                this.BirthDate = BirthDate;
                this.DeathDate = DeathDate;
            }
        }

        struct Timeline
        {
            public DateTime Date;
            public string Description;
            public Timeline(DateTime Date, string Description)
            {
                this.Date = Date;
                this.Description = Description;
            }
        }

        static List<MenuItem> ListAdd()
        {
            List<MenuItem> menuItems = new List<MenuItem>();
            menuItems.Add(new MenuItem("Добавить человека", () => { }));
            menuItems.Add(new MenuItem("Добавить событие", () => { }));
            menuItems.Add(new MenuItem("Изменить данные человека", () => { }));
            menuItems.Add(new MenuItem("Люди, рожденные в високосный год", () => { }));
            menuItems.Add(new MenuItem("Дельта между событиями", () => { }));
            menuItems.Add(new MenuItem("Выход", () => Environment.Exit(0)));

            return menuItems;
        }

        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Menu menu = new Menu(ListAdd());
            menu.MenuItem();

            Person[] peopleDate = null;
            Timeline[] timeline = null;
            Console.WriteLine("Выберете с чем работать: json или csv:");
            var str = Console.ReadLine();
            Console.Clear();
            if (str == "json")
            {
                peopleDate = ReadPersonJSON().ToArray();
                timeline = ReadTimelineJSON().ToArray();
            }
            else if (str == "csv")
            {
                peopleDate = ReadDataPeopleCSV();
                timeline = ReadDataTimelineCSV();
                WritePeopleJSON(peopleDate, "..\\..\\..\\..\\people.json");
                WriteTimelineJSON(timeline, "..\\..\\..\\..\\timeline.json");
            }
            DeltaDate(timeline);
            foreach (var people in GetPeoples(peopleDate))
            {
                Console.WriteLine(people);
            }
        }

        static void WritePeopleJSON(Person[] array, string path)
        {
            string json = JsonConvert.SerializeObject(array, Formatting.Indented);
            File.WriteAllText(path, json);
        }

        static void WriteTimelineJSON(Timeline[] array, string path)
        {
            string json = JsonConvert.SerializeObject(array, Formatting.Indented);
            File.WriteAllText(path, json);
        }

        static List<Person> ReadPersonJSON()
        {
            var jsonPersonString = File.ReadAllText("..\\..\\..\\..\\people.json");
            return JsonConvert.DeserializeObject<List<Person>>(jsonPersonString.Replace(';', ' '));
        }

        static List<Timeline> ReadTimelineJSON()
        {
            var jsonTimelineString = File.ReadAllText("..\\..\\..\\..\\timeline.json");
            return JsonConvert.DeserializeObject<List<Timeline>>(jsonTimelineString.Replace(';', ' '));
        }

        static Timeline[] ReadDataTimelineCSV()
        {
            string pathss = "..\\..\\..\\..\\timeline.csv";
            string[] lines = File.ReadAllLines(pathss);
            Timeline[] splitData = new Timeline[lines.Length];
            int i = 0;

            foreach (var line in lines)
            {
                string[] words = line.Split(';');
                var description = "";
                if (words.Length == 1)
                {
                    description = "";
                }
                else
                {
                    description = words[1];
                }
                splitData[i++] = new Timeline(ParseDate(words[0]), description);
            }
            return splitData;
        }

        static Person[] ReadDataPeopleCSV()
        {
            string pathh = "..\\..\\..\\..\\people.csv";
            string[] lines = File.ReadAllLines(pathh);
            Person[] personInfoSplit = new Person[lines.Length];
            int i = 0;

            foreach (var line in lines)
            {
                string[] words = line.Split(';');
                DateTime deathDate;
                if (words.Length == 3)
                {
                    deathDate = default;
                }
                else
                {
                    deathDate = ParseDate(words[3]);
                }
                personInfoSplit[i++] = new Person(Int32.Parse(words[0]), words[1], ParseDate(words[2]), deathDate);
            }
            return personInfoSplit;
        }

        static DateTime ParseDate(string line)
        {
            DateTime date;
            if (!DateTime.TryParseExact(line, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(line, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(line, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        date = default;
                    }
                }
            }
            return date;
        }

        static DateTime GetMaxDate(Timeline[] timeline)
        {
            DateTime maxDate = DateTime.MinValue;
            foreach (var line in timeline)
            {
                DateTime date = line.Date;
                if (date > maxDate)
                {
                    maxDate = date;
                }
            }
            return maxDate;
        }
        static DateTime GetMinDate(Timeline[] timeline)
        {
            DateTime minDate = DateTime.MaxValue;
            foreach (var line in timeline)
            {
                DateTime date = line.Date;
                if (date < minDate)
                {
                    minDate = date;
                }
            }
            return minDate;
        }
        static void DeltaDate(Timeline[] timeline)
        {
            var maxDate = GetMaxDate(timeline);
            var minDate = GetMinDate(timeline);
            var years = maxDate.Year - minDate.Year;
            var months = maxDate.Month - minDate.Month;
            var days = maxDate.Day - minDate.Day;
            Console.WriteLine($"Между максимальной и минимальной датами прошло: {years} лет, {months} месяцев и {days} дней");
        }
        static List<string> GetPeoples(Person[] people)
        {
            var today = DateTime.Today;
            List<string> result = new List<string>();
            foreach (var line in people)
            {
                var datePeople = line.BirthDate;
                var delta = new DateTime().Add(today.Subtract(datePeople));
                if (DateTime.IsLeapYear(datePeople.Year) && delta.Year <= 20)
                {
                    result.Add(line.Name);
                }
            }
            return result;
        }
    }
}
