﻿namespace TimeTreess
{
    internal class Menu
    {
        List<MenuItem> menuItems = new List<MenuItem>();

        public Menu(List<MenuItem> menuItems)
        {
            this.menuItems = menuItems;
        }

        public void DrawMenu(int row, int col, int index)
        {
            Console.SetCursorPosition(col, row);
            for (int i = 0; i < menuItems.Count; i++)
            {
                if (i == index)
                {
                    Console.BackgroundColor = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Console.WriteLine(menuItems[i]);
                Console.ResetColor();
            }
            Console.WriteLine();
        }

        public void MenuItem()
        {
            Console.WriteLine("Меню");
            Console.WriteLine();

            int row = Console.CursorTop;
            int col = Console.CursorLeft;
            int index = 0;

            while (true)
            {
                DrawMenu(row, col, index);
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < menuItems.Count - 1)
                            index++;
                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                            index--;
                        break;
                    case ConsoleKey.Enter:
                        menuItems[index].action();
                        break;
                }
            }
        }
    }
}
