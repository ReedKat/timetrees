﻿namespace TimeTreess
{
    internal class MenuItem
    {
        public string Name { get; set; }

        public Action action { get; set; }

        public MenuItem(string name, Action action)
        {
            this.Name = name;
            this.action = action;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
