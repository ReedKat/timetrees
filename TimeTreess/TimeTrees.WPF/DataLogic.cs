﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.WPF
{
    internal class DataLogic
    {
        public DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        date = default;

            return date;
        }

        public string OutputFormat(string input)
        {
            if (input.Length == 4) return "yyyy";
            if (input.Length == 7) return "yyyy-MM";
            return "yyyy-MM-dd";
        }

        public Person[] ReadPerson(string path)
        {
            string[] file = File.ReadAllLines(path);
            Person[] peoples = new Person[file.Length - 1];
            for (int i = 1; i < file.Length; i++)
            {
                string[] line = file[i].Split(";");
                Person person = new Person();
                person.Id = Convert.ToInt32(line[0]);
                person.Name = line[1];
                person.Birth = ParseDate(line[2]);
                person.Death = string.IsNullOrEmpty(line[3]) ? null : ParseDate(line[3]);

                peoples[i - 1] = person;
            }

            return peoples;
        }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birth { get; set; }
        public DateTime? Death { get; set; }
    }
}
