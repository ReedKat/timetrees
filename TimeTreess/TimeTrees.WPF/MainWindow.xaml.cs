﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TimeTrees.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<StackPanel, List<(Polyline, Relationship)>> connections = new Dictionary<StackPanel, List<(Polyline, Relationship)>>();
        private Point? _movePoint;
        private bool _isButtonPress = false;
        DataLogic logic = new DataLogic();
        private bool _isFileOpen = false;
        private string _path = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private Polyline CreateLine()
        {
            Polyline line = new Polyline();
            line.Stroke = Brushes.Teal;
            line.StrokeThickness = 3;
            line.StrokeStartLineCap = PenLineCap.Round;
            line.StrokeEndLineCap = PenLineCap.Round;

            return line;
        }

        private StackPanel CreateTextBox()
        {
            StackPanel stackPanel = new StackPanel();
            TextBox textBoxName = NameBox();
            TextBox textBoxBirth = BirthBox();
            TextBox textBoxDeath = DeathBox();
            stackPanel.Children.Add(textBoxName);
            stackPanel.Children.Add(textBoxBirth);
            stackPanel.Children.Add(textBoxDeath);
            connections.Add(stackPanel, new List<(Polyline, Relationship)>());

            return stackPanel;
        }

        private StackPanel CreateTextBox(Person people)
        {
            StackPanel stackPanel = new StackPanel();
            TextBox textBoxName = NameBox();
            TextBox textBoxBirth = BirthBox();
            TextBox textBoxDeath = DeathBox();

            textBoxName.Text = people.Name;
            textBoxBirth.Text = people.Birth.ToString(logic.OutputFormat(people.Birth.ToString()));
            textBoxDeath.Text = people.Death.HasValue ? people.Death.Value.ToString(logic.OutputFormat(people.Death.Value.ToString())) : "";
            if (textBoxDeath.Text == "0001-01-01") textBoxDeath.Text = "";
            stackPanel.Children.Add(textBoxName);
            stackPanel.Children.Add(textBoxBirth);
            stackPanel.Children.Add(textBoxDeath);
            connections.Add(stackPanel, new List<(Polyline, Relationship)>());

            return stackPanel;
        }

        private TextBox NameBox()
        {
            TextBox textBox = new TextBox();
            textBox.Foreground = Brushes.White;
            textBox.Background = Brushes.MediumPurple;
            textBox.BorderBrush = Brushes.MediumPurple;
            textBox.Width = 100;
            textBox.Height = 20;
            textBox.TextAlignment = TextAlignment.Center;
            textBox.ContextMenu = null;
            textBox.IsEnabled = false;

            textBox.KeyDown += (sender, e) =>
            {
                TextBox text = (TextBox)sender;
                if (e.Key == Key.Enter)
                {
                    if (text.Text == "" || text.Text == "Can't be empty")
                    {
                        text.Text = "Can't be empty";
                    }
                    else { text.IsEnabled = false; }
                }
            };

            return textBox;
        }

        private TextBox BirthBox()
        {
            TextBox textBox = new TextBox();
            textBox.Foreground = Brushes.White;
            textBox.Background = Brushes.LightSkyBlue;
            textBox.BorderBrush = Brushes.LightSkyBlue;
            textBox.Width = 120;
            textBox.Height = 20;
            textBox.TextAlignment = TextAlignment.Center;
            textBox.ContextMenu = null;
            textBox.IsEnabled = false;

            textBox.KeyDown += (sender, e) =>
            {
                TextBox text = (TextBox)sender;
                if (e.Key == Key.Enter)
                {
                    DateTime date = logic.ParseDate(textBox.Text);
                    if (date != default) { text.IsEnabled = false; }
                    else { text.Text = "Format yyyy-mm-dd"; }
                }
            };

            return textBox;
        }

        private TextBox DeathBox()
        {
            TextBox textBox = new TextBox();
            textBox.Foreground = Brushes.White;
            textBox.Background = Brushes.LightSkyBlue;
            textBox.BorderBrush = Brushes.LightSkyBlue;
            textBox.Width = 120;
            textBox.Height = 20;
            textBox.TextAlignment = TextAlignment.Center;
            textBox.ContextMenu = null;
            textBox.IsEnabled = false;

            textBox.KeyDown += (sender, e) =>
            {
                TextBox text = (TextBox)sender;
                if (e.Key == Key.Enter)
                {
                    DateTime date = logic.ParseDate(textBox.Text);
                    if (text.Text == "" || date != default) { text.IsEnabled = false; }
                    else { text.Text = "Format yyyy-mm-dd"; }
                }
            };

            return textBox;
        }

        private void CreateFigure_Click(object sender, RoutedEventArgs e)
        {
            _isButtonPress = !_isButtonPress;
            Button button = (Button)sender;
            button.Background = _isButtonPress ? Brushes.Red : Brushes.PaleVioletRed;
            
        }

        private void MainRoot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
            Point point = e.GetPosition(MainRoot);
            if (_isButtonPress != true)
            {
                return;
            }
            StackPanel stackPanel = CreateTextBox();
            MainRoot.Children.Add(stackPanel);

            Canvas.SetLeft(stackPanel, point.X);
            Canvas.SetTop(stackPanel, point.Y);

            stackPanel.MouseLeftButtonDown += FigureMouseDown;
            stackPanel.MouseMove += FigureMouseMove;
            stackPanel.MouseLeftButtonUp += FigureMouseUp;
            stackPanel.MouseRightButtonDown += WriteText;
            stackPanel.MouseRightButtonDown += Connection;
        }

        private void AddStackPanelToCanvas(StackPanel stackPanel)
        {
            MainRoot.Children.Add(stackPanel);

            Canvas.SetLeft(stackPanel, 200);
            Canvas.SetTop(stackPanel, 200);

            stackPanel.MouseLeftButtonDown += (sender, e) => FigureMouseDown(sender, e);
            stackPanel.MouseMove += (sender, e) => FigureMouseMove(sender, e);
            stackPanel.MouseLeftButtonUp += (sender, e) => FigureMouseUp(sender, e);
            stackPanel.MouseRightButtonDown += (sender, e) => WriteText(sender, e);
            stackPanel.MouseRightButtonDown += (sender, e) => Connection(sender, e);
        }

        private void WriteText(object sender, MouseButtonEventArgs e)
        {
            if (ConnectionFigures.connections) return;
            StackPanel stackPanel = (StackPanel)sender;

            foreach (var child in stackPanel.Children)
            {
                if (child is TextBox)
                {
                    TextBox textBox = (TextBox)child;
                    textBox.IsEnabled = true;
                }
            }
        }

        private void RedrawCanvas()
        {
            MainRoot.Children.Clear();

            foreach (var keyValuePair in connections)
            {
                foreach ((Polyline, Relationship) line in keyValuePair.Value)
                {
                    if (!MainRoot.Children.Contains(line.Item1))
                    {
                        MainRoot.Children.Add(line.Item1);
                    }
                }
                MainRoot.Children.Add(keyValuePair.Key);
            }
        }

        private void Connection(object sender, MouseEventArgs args)
        {
            if (ConnectionFigures.connections == false) return;

            Point point = args.GetPosition(MainRoot);

            if (ConnectionFigures.start.X == 0 && ConnectionFigures.start.Y == 0)
            {
                ConnectionFigures.start = point;
                ConnectionFigures.stackPanelFirst = (StackPanel)sender;
            }
            else if (ConnectionFigures.end.X == 0 && ConnectionFigures.end.Y == 0)
            {
                ConnectionFigures.stackPanelLast = (StackPanel)sender;
                ConnectionFigures.end = point;

                Polyline line = CreateLine();

                PointFinder(ConnectionFigures.start, ConnectionFigures.end).ForEach(line.Points.Add);

                if (ConnectionFigures.stackPanelFirst == ConnectionFigures.stackPanelLast)
                {
                    ConnectionFigures.Clear();
                    return;
                }

                foreach ((Polyline, Relationship) lineStart in connections[ConnectionFigures.stackPanelFirst])
                    foreach ((Polyline, Relationship) lineEnd in connections[ConnectionFigures.stackPanelLast])
                        if (lineStart.Item1 == lineEnd.Item1) return;

                connections[ConnectionFigures.stackPanelFirst].Add((line, ConnectionFigures.relate));
                connections[ConnectionFigures.stackPanelLast].Add((line, ConnectionFigures.relate));
                /*if (ConnectionFigures.relate == Relationship.Spouse)
                {

                }
                else
                {

                }*/
                MainRoot.Children.Add(line);
                RedrawCanvas();
                ConnectionFigures.Clear();
            }
        }

        private List<Point> PointFinder(Point start, Point end)
        {
            List<Point> list = new List<Point>();
            list.Add(start);
            double distanceX = Math.Abs(start.X - end.X);
            double distanceY = Math.Abs(start.Y - end.Y);
            if (distanceX > distanceY)
            {
                Point point1 = new Point();
                point1.X = Math.Min(start.X, end.X) + distanceX / 2;
                point1.Y = start.Y;
                Point point2 = new Point();
                point2.X = Math.Max(start.X, end.X) - distanceX / 2;
                point2.Y = end.Y;
                list.Add(point1);
                list.Add(point2);
            }
            else
            {
                Point point1 = new Point();
                point1.Y = Math.Min(start.Y, end.Y) + distanceY / 2;
                point1.X = start.X;
                Point point2 = new Point();
                point2.Y = Math.Max(start.Y, end.Y) - distanceY / 2;
                point2.X = end.X;
                list.Add(point1);
                list.Add(point2);
            }
            list.Add(end);

            return list;
        }

        private void FigureMouseMove(object sender, MouseEventArgs args)
        {
            StackPanel stackPanel = (StackPanel)sender;

            if (_movePoint == null) return;

            Point point = args.GetPosition(MainRoot) - (Vector)_movePoint.Value;

            Canvas.SetLeft(stackPanel, point.X);
            Canvas.SetTop(stackPanel, point.Y);

            foreach ((Polyline, Relationship) line in connections[stackPanel])
            {
                Point furtherPoint = GetFurtherPosition(line.Item1.Points, point);
                line.Item1.Points.Clear();
                PointFinder(point + (Vector)_movePoint.Value, furtherPoint).ForEach(line.Item1.Points.Add); // РАБОТЕТ, НО СТРАННО
            }
        }

        private Point GetFurtherPosition(PointCollection points, Point point)
        {
            Point max = points.First();
            Point min = points.Last();
            double line1 = Math.Sqrt(Math.Pow(point.X - max.X, 2) + Math.Pow(point.Y - max.Y, 2));
            double line2 = Math.Sqrt(Math.Pow(point.X - min.X, 2) + Math.Pow(point.Y - min.Y, 2));

            if (line1 < line2)
            {
                return min;
            }
            else
            {
                return max;
            }
        }

        private void FigureMouseUp(object sender, MouseButtonEventArgs args)
        {
            StackPanel stackPanel = (StackPanel)sender;
            _movePoint = null;
            stackPanel.ReleaseMouseCapture();
        }

        private void FigureMouseDown(object sender, MouseButtonEventArgs args)
        {
            StackPanel stackPanel = (StackPanel)sender;
            _movePoint = args.GetPosition(stackPanel);
            stackPanel.CaptureMouse();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            connections.Clear();
            ConnectionFigures.Clear();
            MainRoot.Children.Clear();
            _isFileOpen = false;
            _isButtonPress = false;
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            if (!_isFileOpen)
            {
                AddToNewFile();
                return;
            }
            AddToThisFile();
        }

        private void AddToThisFile()
        {
            List<string> peoples = StackPanelToString();
            int count = 0;
            StreamWriter streamWriter = new StreamWriter(_path, false);
            streamWriter.WriteLine("Id;Name;Birth;Death;");
            foreach (string people in peoples)
            {
                count++;
                streamWriter.WriteLine(people);
                if (count == people.Length - 1)
                {
                    streamWriter.Write(people);
                }
            }
            streamWriter.Close();
        }

        private void AddToNewFile()
        {
            var peoples = StackPanelToString();
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.FileName = "FamilyTree";
            saveFile.DefaultExt = ".csv";
            saveFile.Filter = "Text documents (.csv)|*.csv";

            Nullable<bool> result = saveFile.ShowDialog();

            if (result == true)
            {
                string filename = saveFile.FileName;
                StreamWriter streamWriter = new StreamWriter(filename);
                streamWriter.WriteLine("Id;Name;Birth;Death;");
                foreach (string people in peoples)
                {
                    streamWriter.WriteLine(people);
                }
                streamWriter.Close();
            }
        }

        private List<string> StackPanelToString()
        {
            int id = 0;
            List<string> person = new List<string>();

            foreach (var elem in connections)
            {
                StringBuilder sb = new StringBuilder();
                id += 1;
                sb.Append(id + ";");

                foreach (var element in elem.Key.Children)
                {
                    TextBox textBox = (TextBox)element;

                    if (string.IsNullOrEmpty(textBox.Text))
                    {
                        sb.Append(" ;");
                    }
                    else
                    {
                        sb.Append(textBox.Text + ";");
                    }
                }

                person.Add(sb.ToString());
            }

            return person;
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Text documents (.csv)|*.csv";
            Nullable<bool> result = openFile.ShowDialog();
            if (result == true)
            {
                _isFileOpen = true;
                _path = openFile.FileName;
                Person[] people = logic.ReadPerson(_path);
                foreach (Person person in people)
                {
                    StackPanel stackPanel = CreateTextBox(person);
                    AddStackPanelToCanvas(stackPanel);
                }
                RedrawCanvas();
            }
        }

        private void CreateChild_Click(object sender, RoutedEventArgs e)
        {
            ConnectionFigures.relate = Relationship.Child;
            ConnectionFigures.connections = true;
        }

        private void CreateSpouse_Click(object sender, RoutedEventArgs e)
        {
            ConnectionFigures.relate = Relationship.Spouse;
            ConnectionFigures.connections = true;
        }
    }
}
