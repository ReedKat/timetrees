﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace TimeTrees.WPF
{
    static class ConnectionFigures
    {
        public static bool connections;
        public static Point start;
        public static Point end;
        public static StackPanel? stackPanelFirst;
        public static StackPanel? stackPanelLast;
        public static Relationship relate; 

        public static void Clear()
        {
            ConnectionFigures.connections = false;
            ConnectionFigures.start = new Point();
            ConnectionFigures.end = new Point();
            ConnectionFigures.stackPanelFirst = null;
            ConnectionFigures.stackPanelLast = null;
        }
    }
}
